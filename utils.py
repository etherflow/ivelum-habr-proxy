import re
import typing

from bs4 import BeautifulSoup
from bs4.element import NavigableString

BAD_TAGS = ['script', 'style', 'code']
LINK_RE = r'https://habr.com([/?=&\w.]+)'

# List of HTML entities bs4 dummit lib doesn't know
EXTRA_HTML_ENTITIES = {'&plus;': '+'}


def process_text_in_bs4tree(tree: BeautifulSoup, func: typing.Callable):
    """ Process each chunk of text in bs4 tree with func """
    for el in list(tree.recursiveChildGenerator()):
        if type(el) == NavigableString and el.parent.name not in BAD_TAGS:
            el.replace_with(func(str(el)))


def process_links(text: str, host: str = ''):
    """ replace domain in links from habr.com to host """
    return re.sub(LINK_RE, r'{}\1'.format(host), text)


def modify_nlength_words(text: str, length: int = 6, symbol: str = '™'):
    """ add symbol to the end of each n-length word in the text """
    word_re = re.compile(r'\b(\w{%d})\b(%s*)' % (length, symbol))
    return re.sub(word_re, r'\1{}'.format(symbol), text)


def process_html(text: str):
    """ process html with modify_nlenght_words """
    for entity, symbol in EXTRA_HTML_ENTITIES.items():
        text = text.replace(entity, symbol)
    doc = BeautifulSoup(text, 'lxml')
    process_text_in_bs4tree(doc, modify_nlength_words)
    return doc.prettify()
