import unittest

from utils import process_links, process_html, modify_nlength_words


class ProcessLinksTest(unittest.TestCase):
    def test_basic(self):
        self.assertEqual(
            process_links("Hubs link: https://habr.com/ru/hubs", 'host'),
            "Hubs link: host/ru/hubs",
        )
        self.assertEqual(
            process_links("<link src='https://habr.com/css/style.css'>", ''),
            "<link src='/css/style.css'>"
        )


html_sample = """<html><head><title>Keebee</title></head><body>
<div>
    <h1>Header text</h1>
    <p>Paragraph with <strong>strong</strong> text.</p>
    Тостер',
</div>
</body></html>"""


class ReplacerTest(unittest.TestCase):
    def test_modify_nlength_words(self):
        examples = [
            ('Some text', 'Some! text!'),
            ('Old world', 'Old world'),
            ('Main level', 'Main! level'),
            ('Good! boy!', 'Good! boy!'),
        ]
        for arg, out in examples:
            self.assertEqual(modify_nlength_words(arg, 4, '!'), out)

    def test_basic(self):
        result = process_html(html_sample)
        self.assertTrue('Keebee™' in result)
        self.assertTrue('strong™' in result)
        self.assertTrue('Header™' in result)
        self.assertTrue('Тостер™' in result)


if __name__ == '__main__':
    unittest.main()
