import http.server
import socketserver
import requests

from utils import process_html, process_links

SERVER = ''
PORT = 8000


class PeculiarProxy(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        habr_page = requests.get(
            'https://habr.com' + self.path,
            allow_redirects=False,
        )
        headers = habr_page.headers
        content_type = headers['Content-Type']

        self.send_response(habr_page.status_code)
        self.send_header('Content-Type', content_type)
        if 'Location' in headers:
            self.send_header('Location', process_links(headers['Location']))
        self.end_headers()

        if 'text/html' in content_type:
            text = process_html(habr_page.text)
            text = process_links(text, '')
            response = bytes(text, 'UTF-8')
        else:
            response = habr_page.content
        self.wfile.write(response)


if __name__ == '__main__':
    socketserver.TCPServer.allow_reuse_address = True
    with socketserver.TCPServer((SERVER, PORT), PeculiarProxy) as httpd:
        print("serving at port", PORT)
        try:
            httpd.serve_forever()
        except KeyboardInterrupt:
            pass
        httpd.server_close()
